using UnityEngine;
using UnityEngine.AI;

public class Follower : MonoBehaviour
{
    [SerializeField] Player player;
    [SerializeField] NavMeshAgent followerAgent;
    void Update()
    {
        followerAgent.destination = player.transform.position;
    }
}
