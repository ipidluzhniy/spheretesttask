using UnityEngine;

public class Camera : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] Vector3 cameraOffset;

    private void LateUpdate()
    {
        var targetPosition = target.position;
        targetPosition.y = transform.position.y;
        targetPosition += cameraOffset;
        transform.position = targetPosition;
    }
}
