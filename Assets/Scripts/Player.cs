using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
    [SerializeField] PlayerInput input;
    [SerializeField] NavMeshAgent playerAgent;
    [SerializeField] float speed;
    void Update()
    {
        var playerInput = input.actions["Movement"].ReadValue<Vector2>();
        var moveVector = new Vector3(playerInput.x, 0f, playerInput.y).normalized;
        playerAgent.Move(moveVector * speed * Time.deltaTime);
    }
}
